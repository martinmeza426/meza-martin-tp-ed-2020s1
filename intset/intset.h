struct IntSetNode;

typedef IntSetNode* IntSet;

// Construye un conjunto de enteros vacio
IntSet emptyIS();

// Permite determinar si un entero pertenece al conjunto
bool belongsIS(int x, IntSet s);

// Agrega un entero al conjunto
void addIS(int x, IntSet& s);

// Remueve un entero del conjunto
void removeIS(int x, IntSet& s);

// Permite determinar la cantidad de elementos
int sizeIS(IntSet s);

// Union de los dos conjuntos
IntSet unionIS(IntSet s1, IntSet s2);

// Interseccion de los dos conjuntos
IntSet insersectIS(IntSet s1, IntSet s2);

// Permite obtener un array con los enteros del conjunto
int* toArrayIS(IntSet s);

// Elimina el conjunto
void destroyIS(IntSet s);

// Emprime los valores del conjunto en standard output
void printIS(IntSet s);

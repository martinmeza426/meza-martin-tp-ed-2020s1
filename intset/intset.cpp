#include <iostream>
#include <string>
#include "intset.h"

struct IntSetNode {
    int elem;
    IntSetNode* next;
};

// Construye un conjunto de enteros vacio
IntSet emptyIS(){
    return NULL;
}

// Permite determinar si un entero pertenece al conjunto
bool belongsIS(int x, IntSet s){
    std::cout << "Falta completar belongsIS" << std::endl;
}

// Agrega un entero al conjunto
void addIS(int x, IntSet& s){
    std::cout << "Falta completar addIS" << std::endl;
}
// Remueve un entero del conjunto
void removeIS(int x, IntSet& s){
    std::cout << "Falta completar removeIS" << std::endl;
}

// Permite determinar la cantidad de elementos
int sizeIS(IntSet s){
    std::cout << "Falta completar sizeIS" << std::endl;
}

// Union de los dos conjuntos
IntSet unionIS(IntSet s1, IntSet s2){
    std::cout << "Falta completar unionIS" << std::endl;
}

// Interseccion de los dos conjuntos
IntSet insersectIS(IntSet s1, IntSet s2){
    std::cout << "Falta completar insersectIS" << std::endl;
    exit(1);
}

// Permite obtener un array con los enteros del conjunto
int* toArrayIS(IntSet s){
    std::cout << "Falta completar toArrayIS" << std::endl;
    exit(1);
}

// Elimina el conjunto
void destroyIS(IntSet& s){
    std::cout << "Falta completar destroyIS" << std::endl;
}

// Emprime los valores del conjunto en standard output
void printIS(IntSet s) {
    std::cout << "Falta completar printIS" << std::endl;
}
